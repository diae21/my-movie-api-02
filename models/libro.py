from config.database import Base
from sqlalchemy import Column, Integer, String, Float

class Libro(Base):

    __tablename__="libros"

    codigo = Column(Integer, primary_key = True)
    titulo = Column(String)
    autor = Column(String)
    ano = Column(Integer)
    categoria = Column(String)
    npag = Column(Integer)
