from fastapi import FastAPI, Body
from fastapi.responses import HTMLResponse
from pydantic import BaseModel, Field
from typing import Optional

#INICIO
app = FastAPI()
app.title = "Mi primera chamba con FastApi"
app.version = "0.0.1"

#Base Computadora
class Computadora(BaseModel):
    id: Optional[int] = None, 
    marca: str = Field( min_length = 2, max_length = 15)
    modelo: str = Field( min_length = 2, max_length = 15)
    color: str = Field( min_length = 4, max_length = 15)
    ram: str = Field( min_length = 3, max_length = 5)
    almacenamiento: str = Field( min_length = 3, max_length = 5)
    
    class Config:
        json_schema_extra = {
            "example" : {
                "id": 1,
                "marca": "HP",
                "modelo": "Pavilion",
                "color": "Gris Plata",
                "ram": "16GB",
                "almacenamiento": "128GB"
            }
        }

#PRECARGA DE COMPUTADORAS
computadoras = [   
    {'id': 1, 'marca': 'Dell', 'modelo': 'Inspiron 15', 'color': 'Plateado', 'ram': '8GB', 'almacenamiento': '512GB SSD'},
    {'id': 2, 'marca': 'HP', 'modelo': 'Pavilion x360', 'color': 'Gris', 'ram': '16GB', 'almacenamiento': '1TB HDD'},
    {'id': 3, 'marca': 'Lenovo', 'modelo': 'ThinkPad X1 Carbon', 'color': 'Negro', 'ram': '16GB', 'almacenamiento': '256GB SSD'},
    {'id': 4, 'marca': 'Asus', 'modelo': 'ROG Strix G', 'color': 'Negro/Rojo', 'ram': '32GB', 'almacenamiento': '1TB SSD'},
    {'id': 5, 'marca': 'HP', 'modelo': 'Pavilion', 'color': 'Gris', 'ram': '8GB', 'almacenamiento': '256GB SSD'}
]

#GET COMPUTADORAS
@app.get('/', tags=['HOME COMPUTADORAS'])
def message():
    return HTMLResponse('<h1> Página de prácticas de Computadoras </h1>')

@app.get('/computadoras', tags=['Get Computadoras'])
def get_computadoras():
    return computadoras

@app.get('/computadoras/{id}', tags=['Get Computadoras'])
def get_computadoras(id: int):
    for item in computadoras:
        if item["id"] == id:
            return item
    return []

@app.get('/computadoras/', tags=['Get Computadoras'])
def get_computadoras_by_marca(marca: str):
    computadorcitas = []
    for computadora in computadoras:
        if computadora['marca'].lower() == marca.lower():
            computadorcitas.append(computadora)
    
    if computadorcitas:
        return computadorcitas
    else:
        return f"No hay computadoras con la marca '{marca}'."
    

#CREATE COMPUTADORAS
@app.post('/computadoras', tags=['Create Computadoras'])
def create_computadora(id: int = Body(), marca: str = Body(), modelo: str = Body(), color: str = Body(), ram: str = Body(), almacenamiento: str = Body()):
    computadoras.append({
        "id":id,
        "marca": marca,
        "modelo":modelo,
        "color":color,
        "ram":ram,
        "almacenamiento":almacenamiento
    })
    return "Se ha creado la computadora exictosamente"


#UPDATE COMPUTADORAS
@app.put('/computadoras/{id}', tags=['Update Computadoras'])
def update_compu(id: int, marca: str = Body(), modelo: str = Body(), color: str = Body(), ram: str = Body(), almacenamiento: str = Body()):
    for item in computadoras:
        if item["id"] == id:
            item["marca"] = marca
            item["modelo"] = modelo
            item["color"] = color
            item["ram"] = ram
            item["almacenamiento"] = almacenamiento
            return computadoras
        

#DELETE COMPUTADORAS
@app.delete('/computadoras/{id}', tags=['Delete Computadoras'])
def delete_compu(id: int):
    for item in computadoras:
        if item["id"] == id:
            computadoras.remove(item)
            return computadoras